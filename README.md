** : **  
**Notes and Assumptions: **  
  1.	Latest Enterprise version of Jira on Feb-25-2020 is 8.5.3.  
  2.	Postgres DB version recommended for above Jira version is 9.6.16.  
  3.	OS used is Linux Ubuntu, 18.04, Image Id: ami-03ba3948f6c37a4b0 and t3.large.  
  4.	DB Passwords not stored in System Manager Parameter store to reduce complexity to run this during evaluation. Username is postgres and password is DBPassword .   
  5.	Stack creation launch expects an existing key value to be selected as parameter.  